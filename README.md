# Elastic-Stack

Trying out the elastic stack.

Getting Started
---------------
1. install docker
2. install docker-compose
3. clone repository: `git clone --recursive git@bitbucket.org:zhao-lin-li/elastic-stack.git`
4. run bootstrap.sh: `./bootstrap.sh`
5. start service: `docker-compose up`

Wait until an event helps autofill the automatically created indices before interacting with kibana and its indices.
If you see something like this in the docker-compose logs:
INFO    [monitoring]    log/log.go:124  Non-zero metrics in the last 30s        {"monitoring": {"metrics": {"beat":{"cpu":{"system":{"ticks":0,"time":0},"total":{"ticks":60,"time":60,"value":60},"user":{"ticks":60,"time":60}},"info":{"ephemeral_id":"676bdf6e-cbdd-405c-a9fe-d78c875165b6","uptime":{"ms":30066}},"memstats":{"gc_next":4194304,"memory_alloc":1656536,"memory_total":3245416,"rss":21524480}},"filebeat":{"harvester":{"open_files":0,"running":0}},"libbeat":{"config":{"module":{"running":1,"starts":1},"reloads":1},"output":{"type":"logstash"},"pipeline":{"clients":2,"events":{"active":0}}},"registrar":{"states":{"current":0},"writes":1},"system":{"cpu":{"cores":4},"load":{"1":3.59,"15":2.6,"5":3.11,"norm":{"1":0.8975,"15":0.65,"5":0.7775}}}}}}
then the app should be ready for normal use.

Surface Testing
---------------
browse to: `http://localhost:5601`
browse to: `http://localhost:9600`
To send events to logstash via http:

    curl -d '{"key1":"value1", "key2":1.5, "source":"http"}' -H "Content-Type: application/json" -X POST http://localhost:8080

To send events to logstash via filebeat:

    echo "{\"key1\":\"value1\", \"key2\":2.5, \"source\":\"file\"}" >> filebeat/files/test.log
    # adding a new line with an editor will result in a new file so every line
    # of the file will be resent to logstash resulting in duplicate events

To add data to the postgres database:

    (host)$ docker-compose run postgres bash
    (postgres)$ psql --username $POSTGRES_USER --host postgres --port 5432 --dbname development
    (psql)$ INSERT INTO development_table (Key1, Key2, source) VALUES ('value2', 2.5, 'postgres');
    (psql)$ UPDATE development_table SET Key2 = 4 WHERE Key1 = 'value2';

To add data to the mongo database:

    (host)$ docker-compose run mongodb bash
    (mongodb)$ mongo mongodb:27017/$MONGODB_DATABASE
    (mongo)$ db.developmentCollection.insert({"_id":"1", "key1":"value2", "key2":2, "source": "mongo"})
    (mongo)$ UPDATE development_table SET Key2 = 4 WHERE Key1 = 'value2';

Manually Creating Timelion Series
---------------------------------

    .es(index=logfile*, timefield=datetime, metric=avg:value).points()
    .es(index=logfile*, timefield=datetime, metric=avg:value).fit(average)

Testing
-------
* TBD

Documenting
-----------
* TBD

Notes
-----
Filebeat is sending through logstash so the auto-generated filebeat index would not have any data unless filebeat sends directly to elasticsearch.
