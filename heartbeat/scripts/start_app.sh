#!/bin/sh

SRC_DIR=$(readlink -f ${0%/*})
. $SRC_DIR/functions.sh

wait_for_service $KIBANA_HOST $KIBANA_PORT

/usr/local/bin/docker-entrypoint -e
