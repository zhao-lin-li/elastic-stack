#!/bin/sh

# wait_for_service
#
# waits for service to be ready for connection
#
# example usage: wait_for_service http://localhost 27017
wait_for_service() {
  local host=$1
  local port=$2

  local service_up=1 #assume service is not up yet
  until [ "$service_up" -eq 0 ]; do
    sleep 5
    nc $host $port </dev/null
    service_up=$?
  done
}
