#!/bin/sh

# This script produces a jar that allows Logstash's JDBC input plugin to connect
# to MongoDB
#
# The key components are:
# - the mongodb JDBC from https://bitbucket.org/dbschema/mongodb-jdbc-driver/overview
# - mongodb's drivers from https://mongodb.github.io/mongo-java-driver/

# get dependencies
curl http://www.dbschema.com/jdbc-drivers/MongoDbJdbcDriver.zip --output MongoDbJdbcDriver.zip
unzip MongoDbJdbcDriver.zip -d MongoDbJdbcDriver
jar -xvf MongoDbJdbcDriver/mongojdbc1.2.jar
jar -xvf MongoDbJdbcDriver/mongo-java-driver-3.4.2.jar

# repackage dependencies
jar -cvf mongo_jdbc.jar com org META-INF

# clean up
rm -rf META-INF com org MongoDbJdbcDriver MongoDbJdbcDriver.zip
